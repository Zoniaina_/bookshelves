import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(){
    var firebaseConfig = {
      apiKey: "AIzaSyDHGaJ5ODSkXMx0r_jIJyLjpi8lsdOitAo",
      authDomain: "bookshelves-deb45.firebaseapp.com",
      databaseURL: "https://bookshelves-deb45.firebaseio.com",
      projectId: "bookshelves-deb45",
      storageBucket: "bookshelves-deb45.appspot.com",
      messagingSenderId: "764757290",
      appId: "1:764757290:web:4cf5da8c28dddb98"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
